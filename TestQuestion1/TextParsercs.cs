﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using NUnit.Framework;

namespace TestQuestion1
{
    [TestFixture]
    public class TextParserText
    {
        [Test]
        public void CanReadFile()
        {
            var textToParse = File.ReadAllText(@"C:\code\TestQuestion1\TestQuestion1\TextFile1.txt");

            Assert.IsNotNull(textToParse);
        }

        [Test]
        public void CanSplitFile()
        {
            var textToParse = File.ReadAllText(@"C:\code\TestQuestion1\TestQuestion1\TextFile1.txt");
            var words = new Regex(@"[\,\^\r\(\n)\(\)\s]").Split(textToParse.Replace("\"", "").Replace(".", "")).ToList();
            var dict = new Dictionary<string, int>();
            foreach (var word in words.Where(x => x != "").ToList())
            {
                if (dict.ContainsKey(word))
                {
                    dict[word] = dict[word] + 1;
                }
                else
                {
                    dict[word] = 1;
                }
            }

            Console.WriteLine("Start");
            foreach (var entry in dict.OrderByDescending(x =>x.Value))
            {
                Console.WriteLine($"{entry.Key} - {entry.Value}");
            }

            Console.WriteLine("End");
        }
    }


}